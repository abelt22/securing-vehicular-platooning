#include <float.h>
#include <stdint.h>

//Serial Port Data Format
#define MINI_HLEN 2 //header length in bytes
#define MINI_PLEN 4 //distance and strength high and low in bytes
#define MINI_MLEN 9 //message length in bytes - is check sum included?
#define MINI_VARS 2 //Number of variables dist and stren

// Globals for TFMini
extern uint8_t MINI_DATA[MINI_PLEN]; //raw data payload
extern uint8_t MINI_I[MINI_VARS]; //inidces into MINI_DATA for vars
extern double MINI_DIST; //raw distance data from TFMini
extern double MINI_STRN; //raw strength data fro  TFMini

// function definitions
extern void UART5_Handler(void); //ISR for UART5
extern void MiniInit(void); //configure TFMini
extern void UART5PreInit(void); // UART config to configure TFMini after reset
extern void UART5PostInit(void); // UART config for after TFMini has been configured (after reset)
extern void UART5Init(void);  //UART config assuming TFMini is configured
extern void MiniConvertData(void); //convert from uint8 to floats (extract vars from buffer)
