// define uC for pin_map.h
#define PART_TM4C123GH6PM

// Keil includes
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "TM4C123GH6PM.h"
// TivaWare includes
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"
#include "inc/hw_gpio.h"
#include "driverlib/debug.h"
#include "driverlib/interrupt.h"
#include "driverlib/uart.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
// VectorNav includes
#include "VN200.h"

// define our global vars
extern double MINI_DIST; //raw distance data from TFMini
extern double MINI_STRN; //raw strength data fro  TFMini


unsigned short calculateCRC(unsigned char data[], unsigned int length)
{
	/*
  unsigned int i;
  unsigned short crc = 0;

	for(i=0; i<length; i++)
	{
		crc  = (unsigned char)(crc >> 8) | (crc << 8);
    crc ^= data[i];
    crc ^= (unsigned char)(crc & 0xff) >> 4;
    crc ^= crc << 12;
    crc ^= (crc & 0x00ff) << 5;
	}
	
	return crc; 
	*/
}



void UART5_Handler(void)
{
	/*
	static uint32_t cnt = 0; //byte index of data frame
	static uint8_t buf[GPS_MLEN]; //where we store frame bytes until copying to global var
	uint8_t b; //for grabbing byte from UART buffer
	uint32_t i; 
	
	UART5->ICR = 0xFFFF; //ack IRQ
	
	//check if IRQ is because of error
	if(UART5->RSR > 0) //error condition
	{
		cnt = 0; //on error we wait until next frame from VectorNav
		UART5->ECR_UART_ALT = 0; //clear error (UARTECR/UARTRSR are the same register)
	}
	
	//grab data and store to buffer (we copy to global var upon complete frame) 
	while(!((UART5->FR >> 4) & 0x1)) //read while buffer not empty
	{
		b = UART5->DR; //grab byte from buffer
		
		//check if we've received an error or are at beginning of frame
		if(cnt == 0 && b == 0xFA) //frame always starts with 0xFA
				cnt++;
		else if(cnt != 0) //we have had valid sync byte; copy data to buffer
		{
			buf[cnt] = b;
			cnt++;
		}
			
		if(cnt == GPS_MLEN) //we've received the entire frame, copy it to var
		{
			//copy data if not corrupted
			if(!calculateCRC(&buf[1], GPS_MLEN))  //check CRC; match returns 0
			{
				//copy should be atomic, disable interrupts
				__set_PRIMASK(1); //mask interrupts
			
				for(i = GPS_HLEN; i < GPS_MLEN; i++)
					GPS_DATA[i] = buf[i];
			
				__set_PRIMASK(0); //unmask interrupts
			}
			
			cnt = 0; //ready for beginning of next frame
		}
	}
	*/
}


void MiniInit(void)
{
	//VN200Cmd(VNRFS); //reset GPS to factory defaults
	//VN200Cmd(VNWRG06); //disable default async output
	//VN200Cmd(VNWRG75); //set our binary output groups
  //VN200Cmd(VNWRG68); //set INS activation velocity threshold
	//VN200Cmd(VNWRG05); //configure UART (slow it down so we don't monopolise uC)
	//VN200Cmd(VNWNV); //store settings to non-volatile memory
}

// UART config to configure TFmini
void UART5PreInit(void)
{
	/*
	//enable clock to GPIO port E
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	//enable clock to UART5
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART5);
	//digital enable and alternative function select
	GPIOPinTypeUART(GPIOE_BASE,0x30); //den and af
	GPIOPinConfigure(GPIO_PE4_U5RX); //pin mux for PE4
	GPIOPinConfigure(GPIO_PE5_U5TX); //pin mux for PE5

	//basic serial parameters: baud rate, word length, stop bits, parity
	UARTConfigSetExpClk(UART5_BASE, SysCtlClockGet(), 115200, 
		(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)); //default VectorNav rate
	//basic serial parameters: baud rate, word length, stop bits, parity
	//UARTConfigSetExpClk(UART5_BASE, SysCtlClockGet(), 57600, 
	//	(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)); //VectorNav rate if we have reconfigured it

	//enable fifo queue
	UARTFIFOEnable(UART5_BASE);
	//enable UART (take care of interrupts after configuring VectorNav)
	UARTEnable(UART5_BASE); 
	*/
}

// UART config for after VectorNav has been configured
void UART5PostInit(void)
{
	/*
	//ensure that above commands have been TX'd before disabling
	while(UARTBusy(UART5_BASE));
	//disable UART during config
	UARTDisable(UART5_BASE);
	//clear all RX errors
	UARTRxErrorClear(UART5_BASE);
	//slow down UART so we don't monopolise uC
	UARTConfigSetExpClk(UART5_BASE, SysCtlClockGet(), 57600, 
		(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
	//enable interrupts for: rx and rx errors
	UARTIntEnable(UART5_BASE,(UART_INT_OE | UART_INT_BE | UART_INT_FE 
		| UART_INT_RT | UART_INT_RX));
	//enable UART interrupt at system level
	UARTIntRegister(UART5_BASE,UART5_Handler);
	//enable UART
	UARTEnable(UART5_BASE);
	*/
}

//assume that VectorNav already configured, setup uC UART to receive async data
void UART5Init(void)
{
	/*
	//enable clock to GPIO port E
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	//enable clock to UART5
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART5);
	//digital enable and alternative function select
	GPIOPinTypeUART(GPIOE_BASE,0x30); //den and af
	GPIOPinConfigure(GPIO_PE4_U5RX); //pin mux for PE4
	GPIOPinConfigure(GPIO_PE5_U5TX); //pin mux for PE5
	//slow down UART so we don't monopolise uC
	UARTConfigSetExpClk(UART5_BASE, SysCtlClockGet(), 57600, 
		(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
	//enable fifo queue
	UARTFIFOEnable(UART5_BASE);
	//enable interrupts for: rx and rx errors
	UARTIntEnable(UART5_BASE,(UART_INT_OE | UART_INT_BE | UART_INT_FE 
		| UART_INT_RT | UART_INT_RX));
	//enable UART interrupt at system level
	UARTIntRegister(UART5_BASE,UART5_Handler);
	//enable UART
	UARTEnable(UART5_BASE);
	*/
}

void MiniConvertData(void)
{
	MINI_DIST = *(double *)&MINI_DATA[MINI_I[0]];
	MINI_STRN = *(double *)&MINI_DATA[MINI_I[1]];
}
